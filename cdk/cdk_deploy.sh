#!/usr/bin/env bash

# Check arguments
if [ "$1" == "" ]; then
    echo "Using DEFAULT AWS Profile"
fi

# constants
THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
LOCAL_TEMP_DIR='_tmp'
CREATOR_LAMBDA_SRC_DIR="${LOCAL_TEMP_DIR}/lambda_creator_src"
CREATOR_LAMBDA_LAYER_DIR="${LOCAL_TEMP_DIR}/lambda_creator_layer"

NUKER_LAMBDA_SRC_DIR="${LOCAL_TEMP_DIR}/lambda_nuker_src"
NUKER_LAMBDA_LAYER_DIR="${LOCAL_TEMP_DIR}/lambda_nuker_layer"

cd $THIS_DIR && \
if [ -d "cdk.out" ]; then
	# delete cdk out because sometimes the local caching can fuck with us and changes will not be detected
	echo "deleting cdk.out"
	rm -r cdk.out
fi && \
# Create folders and fill with content for creator lambda
if [ -d ${CREATOR_LAMBDA_SRC_DIR} ]; then
	echo "Deleting ${CREATOR_LAMBDA_SRC_DIR}"
	rm -r ${CREATOR_LAMBDA_SRC_DIR}
fi && \
if [ -d "${CREATOR_LAMBDA_LAYER_DIR}" ]; then
	echo "Deleting ${CREATOR_LAMBDA_LAYER_DIR}"
	rm -r ${CREATOR_LAMBDA_LAYER_DIR}
fi && \
mkdir -p ${CREATOR_LAMBDA_SRC_DIR} && \
rsync -va ../easy_account_creator/eac.py ${CREATOR_LAMBDA_SRC_DIR} && \
mkdir -p ${CREATOR_LAMBDA_LAYER_DIR}/python && \
pip install -r ../easy_account_creator/requirements.txt -t ${CREATOR_LAMBDA_LAYER_DIR}/python && \
# Create folders and fill with content for nuker lambda
if [ -d "${NUKER_LAMBDA_SRC_DIR}" ]; then
	echo "Deleting ${NUKER_LAMBDA_SRC_DIR}"
	rm -r ${NUKER_LAMBDA_SRC_DIR}
fi && \
if [ -d "${NUKER_LAMBDA_LAYER_DIR}" ]; then
	echo "Deleting ${NUKER_LAMBDA_LAYER_DIR}"
	rm -r ${NUKER_LAMBDA_LAYER_DIR}
fi && \
mkdir -p ${NUKER_LAMBDA_SRC_DIR} && \
rsync -va ../easy_account_nuker_launcher/launcher.py ${NUKER_LAMBDA_SRC_DIR} && \
mkdir -p ${NUKER_LAMBDA_LAYER_DIR}/python && \
pip install -r ../easy_account_nuker_launcher/requirements.txt -t ${NUKER_LAMBDA_LAYER_DIR}/python && \
#
cdk deploy IamRoles CreatorLambda NukerVpc NukerEcs NukerLambda && \
echo "done"
#rm -r "${LOCAL_TEMP_DIR}"
#rm -r ${CREATOR_LAMBDA_SRC_DIR} ${CREATOR_LAMBDA_LAYER_DIR} ${NUKER_LAMBDA_SRC_DIR} ${NUKER_LAMBDA_LAYER_DIR}


