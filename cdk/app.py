#!/usr/bin/env python3.7
import os

from aws_cdk import core

from stacks import (
    iam_stack,
    lambda_creator_stack,
    vpc_nuker_stack,
    ecs_nuker_stack,
    lambda_nuker_stack,
)

##############
# Project Constants
###############
PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
CDK_ROOT = os.path.dirname(os.path.abspath(__file__))

CREATOR_LAMBDA_ROOT = os.path.join(PROJECT_ROOT, "easy_account_creator")
CREATOR_LAMBDA_ENV_PATH = os.path.join(CREATOR_LAMBDA_ROOT, ".env")

CREATOR_LAMBDA_LAYER_LOCAL_PATH = os.path.join(CDK_ROOT, "_tmp", "lambda_creator_layer")
CREATOR_LAMBDA_FUNCTION_LOCAL_PATH = os.path.join(
    CDK_ROOT, "_tmp", "lambda_creator_src"
)

NUKER_LAMBDA_ROOT = os.path.join(PROJECT_ROOT, "easy_account_nuker_launcher")
NUKER_LAMBDA_ENV_PATH = os.path.join(NUKER_LAMBDA_ROOT, ".env")

NUKER_LAMBDA_LAYER_LOCAL_PATH = os.path.join(CDK_ROOT, "_tmp", "lambda_nuker_layer")
NUKER_LAMBDA_FUNCTION_LOCAL_PATH = os.path.join(CDK_ROOT, "_tmp", "lambda_nuker_src")


###############
# User Supplied Constants
###############
VPC_NUKER_CIDR = "10.233.0.0/16"
NUKER_CONTAINER = "wpublic/aws-account-nuker:alpha"


##############
# App
##############
app = core.App()
iam_stack = iam_stack.IamRoleStack(app, "IamRoles")
eac_lambda_stack = lambda_creator_stack.EasyAccountCreatorLambdaStack(
    app,
    "CreatorLambda",
    {
        "iam_stack": iam_stack,
        "layer_path": CREATOR_LAMBDA_LAYER_LOCAL_PATH,
        "lambda_env_path": CREATOR_LAMBDA_ENV_PATH,
        "lambda_src_path": CREATOR_LAMBDA_FUNCTION_LOCAL_PATH,
    },
)
nuker_vpc_stack = vpc_nuker_stack.NukerVpcStack(
    app, "NukerVpc", {"vpc_cidr": VPC_NUKER_CIDR}
)
nuker_ecs_stack = ecs_nuker_stack.NukerEcsStack(
    app,
    "NukerEcs",
    {
        "vpc_stack": nuker_vpc_stack,
        "iam_stack": iam_stack,
        "container_image": NUKER_CONTAINER,
    },
)

nuker_lambda_stack = lambda_nuker_stack.NukerLambdaStack(
    app,
    "NukerLambda",
    {
        "iam_stack": iam_stack,
        "ecs_stack": nuker_ecs_stack,
        "layer_path": NUKER_LAMBDA_LAYER_LOCAL_PATH,
        "lambda_env_path": NUKER_LAMBDA_ENV_PATH,
        "lambda_src_path": NUKER_LAMBDA_FUNCTION_LOCAL_PATH,
    },
)

app.synth()
