# Intro
This is a CDK app that will deploy the Easy Wipable Child Accounts tool into an AWS account. It will use the code from the sibling projects `easy_account_creator` and `easy_account_nuker`

This will deploy
- relevant IAM Roles
- lambda for `easy_account_creator`
- vpc for nuker
- ecs cluster, service and task definition for `easy_account_nuker`
- lambda to launch the `easy_account_nuker` tasks

## Prerequisites
look in `app.py` to see what Constants you need to provide

# VPC Setup
By default we will create a new VPC with CIDR `10.231.0.0/16`. The VPC design is based on the guide here (Practical VPC Design)[https://aws.amazon.com/blogs/startups/practical-vpc-design/]

# Running
Because this requires creating a layer, and this isn't straightforward, we encapsulate zipping necessary files and uploading them via cdk_deploy.sh

```
# lazy create virtual env
./00_py_init.sh
# activate virtual env
source .venv/bin/activate
# run deploy script
AWS_DEFAULT_REGION=us-west-2 AWS_PROFILE=temp_admin ./cdk_deploy.sh CreatorLambda NukerVpc NukerEcs NukerLambda
# deactivate virtualenv if we are done
deactivate
```

## Running individual
```
# here we specify a default region in the command line and a profile in the params
# we only deploy NukerEcs, but because NukerEcs depends on NukerVpc and IamRoles both of those stacks are also deployed
AWS_DEFAULT_REGION=us-west-2 AWS_PROFILE=temp_admin cdk deploy NukerEcs

# this could also be written as
AWS_DEFAULT_REGION=us-west-2 AWS_PROFILE=temp_admin cdk deploy NukerEcs
```


# Notes
- We don't use the built in boto3 in Lambda Python runtime because apparently it is gimped. (source was a technical blog post by someone who tore their hair out trying to figure out why stuff wasn't working)

- Layer signature changes depending on what type of machine dependencies are built. e.g. linux vs macos.

- Container Insights not available for ECS from a CloudFormation template. (source)[https://www.reddit.com/r/aws/comments/cy6f8l/percontainer_ecs_metrics_now_available_on/eypzkli/]. Can only be enabled via CLI on existing cluster.

# Improvements
- Use a docker image mimcing Lambda for creating the layer

- add option to manually control updating of layer (see notes above)

- clean up cdk_deploy.sh script to use functions for repetitive items



# Useful commands

 * `cdk ls`          list all stacks in the app
 * `cdk synth`       emits the synthesized CloudFormation template
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk docs`        open CDK documentation

