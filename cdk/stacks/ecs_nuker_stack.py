from aws_cdk import core, aws_ec2, aws_ecs, aws_logs


class NukerEcsStack(core.Stack):
    def __init__(self, app: core.App, id: str, props={}) -> None:
        super().__init__(app, id)

        vpc_stack = props["vpc_stack"]
        iam_stack = props["iam_stack"]
        container_image = props["container_image"]

        # create cluster
        cluster = aws_ecs.Cluster(self, "NukerCluster", vpc=vpc_stack.vpc)

        # create task definitions
        self.nuker_task = aws_ecs.FargateTaskDefinition(
            self,
            "NukerTask",
            cpu=256,
            memory_limit_mib=512,
            task_role=iam_stack.org_mgr_role,
        )
        self.nuker_container = self.nuker_task.add_container(
            id="NukerContainer",
            image=aws_ecs.ContainerImage.from_registry(container_image),
            logging=aws_ecs.LogDriver.aws_logs(
                stream_prefix="nuker", log_retention=aws_logs.RetentionDays.ONE_MONTH
            ),
        )

        # Create Fargate Service
        self.fargate_service = aws_ecs.FargateService(
            self,
            "NukerService",
            cluster=cluster,
            # desired count is 0 because we don't need anything running at start. we only want something to be running when we say so.
            # if we don't set this to 0, the ecs service deployment gets stuck waiting for the service to stabilize. it won't because our ecs task exits quickly
            # https://aws.amazon.com/premiumsupport/knowledge-center/cloudformation-ecs-service-stabilize/
            desired_count=0,
            task_definition=self.nuker_task,
            vpc_subnets=aws_ec2.SubnetSelection(subnet_type=aws_ec2.SubnetType.PRIVATE),
        )

        core.CfnOutput(self, "NukerFargateClusterName", value=cluster.cluster_name)
        core.CfnOutput(self, "NukerFargateClusterArn", value=cluster.cluster_arn)
        core.CfnOutput(
            self, "NukerFargateServiceName", value=self.fargate_service.service_name
        )
        core.CfnOutput(
            self, "NukerFargateServiceArn", value=self.fargate_service.service_arn
        )
        core.CfnOutput(
            self, "NukerFargateTaskArn", value=self.nuker_task.task_definition_arn
        )
        core.CfnOutput(
            self,
            "NukerFargateTaskContainerName",
            value=self.nuker_container.container_name,
        )

        core.CfnOutput(
            self, "NukerFargateTaskRoleArn", value=self.nuker_task.task_role.role_arn
        )
