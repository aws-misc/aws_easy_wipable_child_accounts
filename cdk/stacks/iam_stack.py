#!/usr/bin/env python3.7
import os

from aws_cdk import aws_iam, core


class IamRoleStack(core.Stack):
    def __init__(self, app: core.App, id: str) -> None:
        super().__init__(app, id)

        org_full_access_policy = aws_iam.ManagedPolicy.from_aws_managed_policy_name(
            "AWSOrganizationsFullAccess"
        )

        # Inline policy to allow access over all
        child_ps = aws_iam.PolicyStatement(
            effect=aws_iam.Effect.ALLOW,
            actions=["sts:AssumeRole"],
            resources=["arn:aws:iam::*:role/OrganizationAccountAccessRole"],
        )
        child_acc_assume_admin_policy = aws_iam.PolicyDocument(
            assign_sids=True, statements=[child_ps]
        )

        org_read_only_policy = aws_iam.ManagedPolicy.from_aws_managed_policy_name(
            "AWSOrganizationsReadOnlyAccess"
        )

        lambda_basic_exec_policy = aws_iam.ManagedPolicy.from_aws_managed_policy_name(
            "service-role/AWSLambdaBasicExecutionRole"
        )

        ecs_ps = aws_iam.PolicyStatement(
            effect=aws_iam.Effect.ALLOW,
            actions=["ecs:RunTask", "ecs:StartTask", "ecs:StopTask"],
            resources=["*"],
        )
        ecs_run_task_policy = aws_iam.PolicyDocument(
            assign_sids=True, statements=[ecs_ps]
        )

        self.org_mgr_role = aws_iam.Role(
            self,
            "OrgMgrRole",
            assumed_by=aws_iam.ServicePrincipal("lambda.amazonaws.com"),
            managed_policies=[org_full_access_policy],
            inline_policies=[child_acc_assume_admin_policy],
            path="/cdk/aws_easy_wipable_child_accounts/",
        )
        # we need to do this separetely because Role's stupid param only allows one "assumed_by" value
        self.org_mgr_role.assume_role_policy.add_statements(
            aws_iam.PolicyStatement(
                actions=["sts:AssumeRole"],
                principals=[aws_iam.ServicePrincipal("ecs-tasks.amazonaws.com")],
            )
        )

        self.lambda_nuker_role = aws_iam.Role(
            self,
            "LambdaNukerRole",
            assumed_by=aws_iam.ServicePrincipal("lambda.amazonaws.com"),
            managed_policies=[org_read_only_policy, lambda_basic_exec_policy],
            inline_policies=[ecs_run_task_policy, child_acc_assume_admin_policy],
            path="/cdk/aws_easy_wipable_child_accounts/",
        )

        # CloudFront Outputs
        core.CfnOutput(
            self, "OrganizationManagerRoleArn", value=self.org_mgr_role.role_arn
        )
        core.CfnOutput(
            self, "OrganizationManagerRoleName", value=self.org_mgr_role.role_name
        )
        core.CfnOutput(
            self, "LambdaNukerRoleArn", value=self.lambda_nuker_role.role_arn
        )
        core.CfnOutput(
            self, "LambdaNukerRoleName", value=self.lambda_nuker_role.role_name
        )
