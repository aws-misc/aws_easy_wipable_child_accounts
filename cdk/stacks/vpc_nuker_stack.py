import json

from aws_cdk import core
from aws_cdk.aws_ec2 import SubnetConfiguration, SubnetType, Vpc


DEFAULT_VPC_CIDR = "10.233.0.0/16"


class NukerVpcStack(core.Stack):
    def __init__(self, app: core.App, id: str, props={}) -> None:
        super().__init__(app, id)

        vpc_cidr = props.get("vpc_cidr", DEFAULT_VPC_CIDR)
        subnet_cidr_mask = props.get("subnet_cidr_mask", 18)

        public_subn = SubnetConfiguration(
            name="NukerPublic",
            subnet_type=SubnetType.PUBLIC,
            cidr_mask=subnet_cidr_mask,
        )
        private_subn = SubnetConfiguration(
            name="NukerPrivate",
            subnet_type=SubnetType.PRIVATE,
            cidr_mask=subnet_cidr_mask,
        )

        self.vpc = Vpc(
            self,
            "NukerVpc",
            cidr=vpc_cidr,
            subnet_configuration=[public_subn, private_subn],
        )

        core.CfnOutput(self, "VpcNukerId", value=self.vpc.vpc_id)
        core.CfnOutput(self, "VpcNukerCIDR", value=self.vpc.vpc_cidr_block)
        private_subnet_ids = [sn.subnet_id for sn in self.vpc.private_subnets]
        core.CfnOutput(
            self, "VpcNukerPrivateSubnetsJson", value=json.dumps(private_subnet_ids)
        )
