from aws_cdk import aws_lambda, core

from dotenv import dotenv_values


class NukerLambdaStack(core.Stack):
    def __init__(self, app: core.App, id: str, props={}) -> None:
        super().__init__(app, id)

        env_data = get_lambda_envs(props["lambda_env_path"])

        layer = aws_lambda.LayerVersion(
            self,
            "EasyAccountNukerLayer",
            code=aws_lambda.AssetCode(props["layer_path"]),
            compatible_runtimes=[aws_lambda.Runtime.PYTHON_3_7],
        )

        lambda_func = aws_lambda.Function(
            self,
            "EasyAccountNukerFunction",
            code=aws_lambda.AssetCode.from_asset(props["lambda_src_path"]),
            runtime=aws_lambda.Runtime.PYTHON_3_7,
            handler="eac.lambda_handler",
            layers=[layer],
            timeout=core.Duration.seconds(300),
            role=props["iam_stack"].org_mgr_role,
            environment=env_data,
        )

        # CloudFront Outputs
        core.CfnOutput(self, "LambdaCreatorName", value=lambda_func.function_name)
        core.CfnOutput(self, "LambdaCreatorArn", value=lambda_func.function_arn)
        core.CfnOutput(
            self, "LambdaCreatorLatestVersion", value=lambda_func.latest_version.version
        )
        core.CfnOutput(
            self, "LambdaLayerCreatorLatestVersion", value=layer.layer_version_arn
        )


def get_lambda_envs(path):
    data = dotenv_values(path)
    # we don't use profile when running in cloud so delete this env
    data.pop("AWS_PROFILE", None)
    return data
