import argparse
import json
import logging
import os
import pprint
import sys
import traceback

import boto3
from botocore.exceptions import ClientError

logger = logging.getLogger()

if __name__ == "__main__":
    from dotenv import load_dotenv

    load_dotenv()

pp = pprint.PrettyPrinter(indent=4)


####### USER SUPPLIED VALUES ##############
# This is the name of the profile in the ~/.aws/credentials file
AWS_PROFILE = os.getenv("AWS_PROFILE", "")
MAX_ACCOUNTS = int(os.getenv("MAX_ACCOUNTS", 0))
EMAIL_FORMAT = os.getenv("EMAIL_FORMAT")

IAM_USER_FORMAT = os.getenv("IAM_USER_FORMAT", "child_admin{:02d}")
IAM_PASSWORD_PREFIX = os.getenv("IAM_PASSWORD_PREFIX")

ROOT_ORGANIZATION_ID = os.getenv("ROOT_ORGANIZATION_ID")
CHILD_ACC_ORGANIZATION_ID = os.getenv("CHILD_ACC_ORGANIZATION_ID")

FRIENDLY_NAME_PREFIX = os.getenv("FRIENDLY_NAME_PREFIX")

######## CONSTANTS #####################

# string template for role that admins a child account
ORGANIZATION_ADMIN_ROLE_TEMPLATE = "arn:aws:iam::{}:role/OrganizationAccountAccessRole"
AWS_ADMINISTRATOR_MANAGED_POLICY_ARN = "arn:aws:iam::aws:policy/AdministratorAccess"
ACCOUNT_SIGNIN_URL_FORMAT = "https://{}.signin.aws.amazon.com/console"


def lambda_handler(event, context):
    try:
        action = event.get("action", "lazy_create_accounts")
        runner(action)
    except Exception as e:
        logger.error("{}".format(e), exc_info=True)
        tb = sys.exc_info()[2]
        tb_str = "".join(traceback.format_tb(tb))


def runner(action):
    acc_list = generate_account_list()

    try:
        if action == "lazy_create_accounts":
            session = get_session()
            org_client = get_org_client(session)
            print("Lazy Creating Accounts...")
            lazy_create_accounts(org_client, acc_list)
            print("Lazy Setting Accounts Aliases...")
            set_account_aliases(session, org_client, acc_list)
            print("Creating IAM Users...")
            set_iam_users(session, org_client, acc_list)
        elif action == "list_acc":
            session = get_session()
            client = get_org_client(session)
            list_accounts(client)
        elif action == "check_acc_create":
            session = get_session()
            client = get_org_client(session)
            account_create_status(client, acc_list)
    except ClientError as err:
        print("----- ERROR ----")
        print(err)
        print("-----")


### Meat Functions
def lazy_create_accounts(client, account_list):
    accounts_in_root = list_accounts(client, ROOT_ORGANIZATION_ID)
    move_accounts(
        client,
        [v for k, v in accounts_in_root.items()],
        [a["account_name"] for a in account_list],
    )

    existing_accounts_dict = list_accounts(client, CHILD_ACC_ORGANIZATION_ID)
    accounts_to_create_list = []
    for a in account_list:
        if a["account_name"] in existing_accounts_dict:
            print("{} exists".format(a["account_name"]))
        else:
            accounts_to_create_list.append(a)

    create_accounts(client, accounts_to_create_list)

    if accounts_to_create_list:
        print("Wait a while for accounts to be created.")
        exit(0)


def list_accounts(client, parent_id):
    results = {}
    next_token = None
    while True:
        # list_accounts_for_parents doesn't take kindly to None value for NextToken
        params = {"ParentId": parent_id}
        if next_token:
            params["NextToken"] = next_token

        response = client.list_accounts_for_parent(**params)
        for a in response["Accounts"]:
            results[a["Name"]] = a
        next_token = response.get("NextToken", None)
        if not next_token:
            break

    return results


def create_accounts(client, account_list):
    results = {}
    for acc in account_list:
        print("creating {}...".format(acc["account_name"]))
        response = client.create_account(
            Email=acc["email"],
            AccountName=acc["account_name"],
            IamUserAccessToBilling="DENY",
        )
        payload = {acc["email"]: {"req_id": response["CreateAccountStatus"]["Id"]}}
        # print_response(payload)
        results.update(payload)


def move_accounts(client, account_list, acc_name_filter=[]):
    for acc in account_list:
        if acc_name_filter:
            if acc["Name"] not in acc_name_filter:
                continue

        print("Moving account {} from root to OU".format(acc["Name"]))
        res = client.move_account(
            AccountId=acc["Id"],
            SourceParentId=ROOT_ORGANIZATION_ID,
            DestinationParentId=CHILD_ACC_ORGANIZATION_ID,
        )


def account_create_status(client, account_list):
    results = {}
    with open(CREATE_ACC_RESULTS_FILE, "r") as f:
        results = json.load(f)

    for email, r in results.items():
        req_id = r["req_id"]
        res = client.describe_create_account_status(CreateAccountRequestId=req_id)
        status_dict = res["CreateAccountStatus"]
        payload = {
            email: {
                "req_id": status_dict["Id"],
                "state": status_dict["State"],
                "detail": status_dict.get("FailureReason", None),
            }
        }
        print_response(payload)


def set_account_aliases(session, client, acc_list):
    existing_accounts_dict = list_accounts(client, CHILD_ACC_ORGANIZATION_ID)

    for a in acc_list:
        existing_acc = existing_accounts_dict.get(a["account_name"], None)
        if not existing_acc:
            raise ("{} not found".format(a["account_name"]))

        print("{} - setting alias {}".format(a["account_name"], a["account_alias"]))
        lazy_set_account_alias(session, existing_acc["Id"], a["account_alias"])


def lazy_set_account_alias(session, account_id, alias):
    target_role = ORGANIZATION_ADMIN_ROLE_TEMPLATE.format(account_id)
    sts_client = get_sts_client(session)
    res = sts_client.assume_role(
        RoleArn=target_role, RoleSessionName="temp_{}".format(alias)
    )
    acc_session = get_session(
        token=res["Credentials"]["SessionToken"],
        access_key_id=res["Credentials"]["AccessKeyId"],
        secret_access_key=res["Credentials"]["SecretAccessKey"],
    )

    acc_iam_client = get_iam_client(acc_session)
    res = acc_iam_client.list_account_aliases()
    try:
        existing_alias = res["AccountAliases"][0]
        # if the alias is different, we need to delete it.
        if not alias == existing_alias:
            acc_iam_client.delete_account_alias(AccountAlias=existing_alias)
        else:
            # if they are the same we are done.
            print("Signin URL:", ACCOUNT_SIGNIN_URL_FORMAT.format(alias))
            return
    except IndexError:
        # means no aliases set
        pass

    # AWS Account aliases must be lowercase per docs
    res = acc_iam_client.create_account_alias(AccountAlias=alias.lower())
    print("Signin URL:", ACCOUNT_SIGNIN_URL_FORMAT.format(alias))


def set_iam_users(session, client, acc_list):
    existing_accounts_dict = list_accounts(client, CHILD_ACC_ORGANIZATION_ID)

    for a in acc_list:
        existing_acc = existing_accounts_dict.get(a["account_name"], None)

        if not existing_acc:
            raise ("{} not found".format(a["account_name"]))

        print(
            '{} - Creating IAM User "{}"'.format(a["account_name"], a["iam_admin_user"])
        )
        lazy_create_iam_users(
            session, existing_acc["Id"], a["iam_admin_user"], a["iam_admin_password"]
        )


def lazy_create_iam_users(session, account_id, username, password):
    target_role = ORGANIZATION_ADMIN_ROLE_TEMPLATE.format(account_id)
    sts_client = get_sts_client(session)
    res = sts_client.assume_role(
        RoleArn=target_role, RoleSessionName="temp_{}".format(username)
    )
    acc_session = get_session(
        token=res["Credentials"]["SessionToken"],
        access_key_id=res["Credentials"]["AccessKeyId"],
        secret_access_key=res["Credentials"]["SecretAccessKey"],
    )

    acc_iam_client = get_iam_client(acc_session)

    # create user
    try:
        res = acc_iam_client.create_user(UserName=username)
        print("created user", res["User"])
    except acc_iam_client.exceptions.EntityAlreadyExistsException as e:
        print("User already exists", username)
        pass

    # set password
    try:
        res = acc_iam_client.create_login_profile(
            UserName=username, Password=password, PasswordResetRequired=False
        )
    except acc_iam_client.exceptions.EntityAlreadyExistsException as e:
        res = acc_iam_client.update_login_profile(
            UserName=username, Password=password, PasswordResetRequired=False
        )
    print("password set successfully")

    # attach admin role
    res = acc_iam_client.attach_user_policy(
        UserName=username, PolicyArn=AWS_ADMINISTRATOR_MANAGED_POLICY_ARN
    )
    print("Attached admin policy")


### Helpers


def generate_account_list():
    acc_list = []
    for i in range(1, MAX_ACCOUNTS + 1):
        account_alias = "{}-ws-acc{:02d}".format(FRIENDLY_NAME_PREFIX, i)

        acc_list.append(
            {
                "email": EMAIL_FORMAT.format(i),
                "account_name": "Workshop{:02d}".format(i),
                "account_alias": account_alias,
                "account_login_url": ACCOUNT_SIGNIN_URL_FORMAT.format(account_alias),
                "iam_admin_user": IAM_USER_FORMAT.format(i),
                "iam_admin_password": "{}{:02d}".format(IAM_PASSWORD_PREFIX, i),
            }
        )

    return acc_list


### AWS Helpers


def get_session(token=None, access_key_id=None, secret_access_key=None):
    """Get a default session, or one from a token, access_key_id and secret_access_key typically supplied by STS.assume_role
    """
    if token or access_key_id or secret_access_key:
        # all three must be present
        if not token or not access_key_id or not secret_access_key:
            raise Error(
                "token, access_key_id and secret_access_key parameters must all be present if any one is present"
            )
        return boto3.Session(
            aws_access_key_id=access_key_id,
            aws_secret_access_key=secret_access_key,
            aws_session_token=token,
        )
    elif AWS_PROFILE:
        return boto3.Session(profile_name=AWS_PROFILE)
    else:
        return boto3.Session()


def get_org_client(session):
    return session.client("organizations")


def get_workmail_client(session):
    return session.client("workmail")


def get_sts_client(session):
    return session.client("sts")


def get_iam_client(session):
    return session.client("iam")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("action")
    args = parser.parse_args()

    runner(args.action)
