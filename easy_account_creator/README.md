# Improvements

- make a set of Service Control Policies to limit what users can do when creating child accounts
	- limit regions
		- only us
		- only eu
		- only ap
	- limit actions
		- no ri
		- no super expensive machines
		- no sql server/ oracle db
		- no windows machines