import argparse
import base64
import json
import logging
import os
import sys
import traceback
from typing import List, Dict, Optional

import boto3

if __name__ == "__main__":
    from dotenv import load_dotenv

    load_dotenv()

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

####################
# Program Constants
ORGANIZATION_ADMIN_ROLE_TEMPLATE = "arn:aws:iam::{}:role/OrganizationAccountAccessRole"
CLUSTER_NAME = os.getenv("CLUSTER_NAME")
TASK_DEFN = os.getenv("TASK_DEFN")
SUBNET_IDS = json.loads(os.getenv("SUBNET_IDS_JSON"))
CONTAINER_NAME = os.getenv("CONTAINER_NAME")
CHILD_ACC_ORGANIZATION_ID = os.getenv("CHILD_ACC_ORGANIZATION_ID")
REGIONS = ["us-east-2", "us-east-1", "ap-southeast-1", "ap-southeast-2", "us-west-1", "us-west-2", "eu-west-1", "eu-west-2"]


########################
# Lambda handler

def lambda_handler(event, context):
    try:
        # action = event.get("action", "nuke_accounts")
        runner()
    except Exception as e:
        logger.error("{}".format(e), exc_info=True)
        tb = sys.exc_info()[2]
        tb_str = "".join(traceback.format_tb(tb))


####################################################
# AWS HELPERS
def get_current_aws_account_id(session:boto3.session.Session)->str:
    sts_client = boto3.client('sts')
    return sts_client.get_caller_identity().get('Account')


def list_accounts(client:boto3.client, parent_id:str)->Dict:
    results = {}
    next_token = None
    while True:
        # list_accounts_for_parents doesn't take kindly to None value for NextToken
        params = {"ParentId": parent_id}
        if next_token:
            params["NextToken"] = next_token

        response = client.list_accounts_for_parent(**params)
        for a in response["Accounts"]:
            results[a["Name"]] = a
        next_token = response.get("NextToken", None)
        if not next_token:
            break

    return results


def get_org_client(session:boto3.session.Session)-> boto3.client:
    return session.client("organizations")


def get_child_acc_credentials(session:boto3.session.Session, account_id:str)-> Dict:
    role_arn = ORGANIZATION_ADMIN_ROLE_TEMPLATE.format(account_id)
    sts_client = session.client('sts')
    # set a default session expiry of 1200
    creds = sts_client.assume_role(RoleArn=role_arn, RoleSessionName='nuker-task-session', DurationSeconds=1200)

    return creds

def get_acc_alias(session:boto3.session.Session) -> str:
    iam_client = session.client('iam')
    res = iam_client.list_account_aliases()
    try:
        return res['AccountAliases'][0]
    except (KeyError, IndexError) as err:
        raise Error('Error Getting Account Alias using {}'.format(session.get_credentaials()))


def runner() -> None:

    session =  boto3.Session()
    current_aws_account_id = get_current_aws_account_id(session)

    org_client = get_org_client(session)
    results = list_accounts(org_client, CHILD_ACC_ORGANIZATION_ID)
    logger.debug('accounts data:\n{}'.format(results))


    for k,v in results.items():
        child_creds = get_child_acc_credentials(session, v['Id'])
        child_acc_session = boto3.Session(
            aws_access_key_id=child_creds['Credentials']['AccessKeyId'],
            aws_secret_access_key=child_creds['Credentials']['SecretAccessKey'],
            aws_session_token = child_creds['Credentials']['SessionToken']
        )
        child_acc_alias = get_acc_alias(child_acc_session)

        logger.info('Launching Nuke at {}'.format(child_acc_alias))
        logger.debug('child_creds:\n{}'.format(child_creds))


        for r in REGIONS:
            nuker_config = {
                'regions': [r,],
                'account-blacklist': [current_aws_account_id,],
                'accounts': {v['Id']: {}},
            }

            _nuker_config_json = json.dumps(nuker_config)
            b64_data = base64.b64encode(_nuker_config_json.encode())

            # Action is not used currently.
            params = {
                "cluster_name": CLUSTER_NAME,
                "task_defn": TASK_DEFN,
                "subnet_ids": SUBNET_IDS,
                "container_name": CONTAINER_NAME,
                "task_env": [
                    {"name": "NUKER_DATA", "value": str(b64_data, 'utf-8'),},
                    {"name": "AWS_ACCESS_KEY_ID", "value": child_creds['Credentials']['AccessKeyId'],},
                    {"name": "AWS_SECRET_ACCESS_KEY", "value": child_creds['Credentials']['SecretAccessKey'],},
                    {"name": "AWS_SESSION_TOKEN", "value": child_creds['Credentials']['SessionToken'],},
                    {"name": "AWS_ACCOUNT_ALIAS", "value": child_acc_alias,},
                ],
            }
            logger.debug("params: {}".format(json.dumps(str(params), indent=4)))
            res = run_nuker_task(**params)
            logger.info(res)


    # get list of all child accounts under OU
    # for each account:
        # get account alias
        # get session token for child account
        # get account id
        # get this (parent) account id

    #return




def run_nuker_task(
    cluster_name: str,
    task_defn: str,
    subnet_ids: List,
    container_name: str,
    task_env: List,
) -> Dict:
    client = boto3.client("ecs")
    response = client.run_task(
        cluster=cluster_name,
        launchType="FARGATE",
        taskDefinition=task_defn,
        count=1,
        platformVersion="LATEST",
        networkConfiguration={
            "awsvpcConfiguration": {
                "subnets": subnet_ids,
                #'securityGroups': [],
            }
        },
        overrides={
            "containerOverrides": [{"name": container_name, "environment": task_env}]
        },
    )
    return response


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    #parser.add_argument("action")
    args = parser.parse_args()

    # runner(args.action)
    runner()




