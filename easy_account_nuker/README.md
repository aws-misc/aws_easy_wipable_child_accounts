# Introduction

This is used to nuke a child account. The image is available at `wpublic/aws-account-nuker:alpha` or you can upload it to your own repo.

It can be run standalone as long as relevant parameters are provided to `nuker.py`

Requires permission so that it can assume `OrganizationAccountAccessRole` in each of the child accounts. (source)[https://docs.aws.amazon.com/organizations/latest/userguide/orgs_manage_accounts_access.html#orgs_manage_accounts_access-cross-account-role]




# PreReq

Manually download the latest release for aws-nuker. [https://github.com/rebuy-de/aws-nuke/releases/latest]

Modify the DockerFile to also copy this file into the image. e.g.
```
ADD aws-nuke-v2.12.0-linux-amd64 /
```



# Build the Docker
```
docker build -t nuker .
```

## Convenient Build, Tag and Push
Thsi one liner will build an image, tag it and then push it to the container repository

```
docker build -t nuker . && docker tag nuker:latest wpublic/aws-account-nuker:alpha && docker push wpublic/aws-account-nuker:alpha
```


# Running Locally
```
# assuming the image is tagged 'nuker'
docker run nuker python nuker.py --use_internal

# if the image is tagged with version 'alpha'
docker run nuker:alpha python nuker.py --use_internal
```