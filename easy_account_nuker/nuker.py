import argparse
import base64
import json
import logging
import os
import subprocess
import sys
import traceback

import requests
import yaml


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

##################################################
# Program Constants
#
CONFIG_FILENAME = "config.yml"
# When running on MacOS make sure to use darwin build
NUKER_BIN = "aws-nuke-v2.12.0-linux-amd64"
ENV_DATA_KEY = "NUKER_DATA"

##################################################
# Internal Data for running locally
#
_nuker_config_data = {
    "regions": [
        "us-east-2",
        "us-east-1",
        "ap-southeast-1",
        "ap-southeast-2",
        "us-west-1",
        "us-west-2",
        "eu-west-1",
        "eu-west-2",
    ],
    "account-blacklist": ["429407425143"],
    "accounts": {"045058614004": {}},
}
_nuker_config_json = json.dumps(_nuker_config_data)
B64_DATA = base64.b64encode(_nuker_config_json.encode())


##################################################
# App Logic
#
def decode_params(b64_data):
    param_str = base64.b64decode(b64_data)
    return json.loads(param_str)


def write_config_file(config_file, param_dict):
    # based on environment varialbes write a config file
    with open(config_file, "w") as f:
        yaml.dump(param_dict, f)


def get_nuke_command(
    nuker_binary,
    config_filename,
    *,
    profile_name=None,
    access_key_id=None,
    secret_access_key=None,
    session_token=None
):

    cmd = ["./{}".format(nuker_binary), "--config", config_filename, "--no-dry-run"]

    if profile_name:
        cmd.extend(["--profile", profile_name])
    elif access_key_id or secret_access_key:
        if not access_key_id or not secret_access_key:
            raise Error("Both access_key_id and secret_access_key must be provided")
        cmd.extend(
            [
                "--access-key-id",
                access_key_id,
                "--secret-access-key",
                secret_access_key,
            ]
        )
        if session_token:
            cmd.extend([
                "--session-token",
                session_token,
            ])

    logger.debug("NUKE CMD:\n{}".format(cmd))

    return cmd


def get_container_credentials(credentials_relative_uri):
    cred_url = "http://169.254.170.2{}".format(credentials_relative_uri)
    res = requests.get(cred_url)
    data = res.json()
    return {
        "access_key_id": data["AccessKeyId"],
        "secret_access_key": data["SecretAccessKey"],
        "session_token": data["Token"],
    }


def run_nuke(nuker_binary, config_filename, account_alias):
    logger.debug("RUNNING")

    # Assume that if we have AWS_PROFILE defined, we are running locally and want to use that
    if os.getenv("AWS_PROFILE", None):
        data = {"profile_name": os.getenv("AWS_PROFILE")}
    # otherwise we are running in a fargate container and want to use AWS_CONTAINER_CREDENTIALS_RELATIVE_URI
    elif os.getenv("AWS_ACCESS_KEY_ID", None) and os.getenv(
        "AWS_SECRET_ACCESS_KEY", None
    ):
        data = {
            "access_key_id": os.getenv("AWS_ACCESS_KEY_ID"),
            "secret_access_key": os.getenv("AWS_SECRET_ACCESS_KEY"),
        }
        token = os.getenv("AWS_SESSION_TOKEN")
        if token:
            data['session_token'] = token
    else:
        raise Error(
            "NO Credentials Provided. Require env variable AWS_PROFILE or both AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY to be set. "
        )

    nuker_cmd_params = {
        "nuker_binary": nuker_binary,
        "config_filename": config_filename,
    }
    nuker_cmd_params.update(data)

    nuke_command = get_nuke_command(**nuker_cmd_params)
    with open("test.log", "wb") as f:
        process = subprocess.Popen(
            nuke_command,
            stdin=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            stdout=subprocess.PIPE,
        )
        for line in iter(process.stdout.readline, b""):
            # Write log both to std out and to log file
            decoded_line = line.decode(sys.stdout.encoding)
            sys.stdout.write(decoded_line)
            f.write(line)

            # logix
            if "Enter account alias to continue" in decoded_line:
                # blah, errs = process.communicate(input='wlab-wsu01\n'.encode(sys.stdout.encoding))
                process.stdin.write(
                    "{}\n".format(account_alias).encode(sys.stdout.encoding)
                )
                process.stdin.flush()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--use_internal",
        help="Use the hardcoded values internally",
        action="store_true",
    )
    args = parser.parse_args()
    logger.debug("args: {}".format(args))

    try:
        if args.use_internal:
            param_dict = decode_params(B64_DATA)
        else:
            env_b64_data = os.getenv(ENV_DATA_KEY)
            param_dict = decode_params(env_b64_data)

        logger.debug("paramdict:\n{}".format(json.dumps(param_dict, indent=4)))

        account_alias = os.getenv('AWS_ACCOUNT_ALIAS')
        if not account_alias:
            raise Error('Account Alias missing. It must be supplied via env variable AWS_ACCOUNT_ALIAS')

        write_config_file(CONFIG_FILENAME, param_dict)
        run_nuke(NUKER_BIN, CONFIG_FILENAME, account_alias)
    except Exception as err:
        logger.error("""--------- !! ERROR !! --------

            {}

            --------------------------
            """.format(err), exc_info=True)
        tb = sys.exc_info()[2]
        tb_str = "".join(traceback.format_tb(tb))

    logger.debug("------------ Done ------------\n\n")
