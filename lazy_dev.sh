#!/usr/bin/env bash

SESSION_NAME='EasyWipableChildAccounts'
THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
CDK_DIR="$THIS_DIR/cdk"
CREATOR_LAMBDA_DIR="$THIS_DIR/easy_account_creator"
NUKER_LAMBDA_DIR="$THIS_DIR/easy_account_nuker_launcher"
NUKER_TASK_DIR="$THIS_DIR/easy_account_nuker"

KILL_ARG='kill'


if [ "$1" == "$KILL_ARG" ]
then
tmux kill-session -t $SESSION_NAME
echo "$SESSION_NAME Session Killed: $?"
exit 0
fi

tmux attach -t $SESSION_NAME

if [ $? -eq 0 ]
then
exit 0
fi

cd ~

# if we get here, we need to create our session
tmux new-session -d -s $SESSION_NAME
tmux rename-window 'xxx'
tmux send-keys "cd $API_DIR" C-m


tmux new-window -t $SESSION_NAME:1 -n 'cdk'
tmux select-window -t $SESSION_NAME:cdk
tmux send-keys "cd $CDK_DIR" C-m "./00_py_init.sh" C-m "source .venv/bin/activate" C-m


tmux new-window -t $SESSION_NAME:2 -n 'creator'
tmux select-window -t $SESSION_NAME:creator
tmux send-keys "cd $CREATOR_LAMBDA_DIR" C-m "./00_py_init.sh" C-m "source .venv/bin/activate" C-m


tmux new-window -t $SESSION_NAME:3 -n 'easy-acc-nuker-launcher'
tmux select-window -t $SESSION_NAME:nuker-launcher
tmux send-keys "cd $NUKER_LAMBDA_DIR" C-m "./00_py_init.sh" C-m "source .venv/bin/activate" C-m



tmux new-window -t $SESSION_NAME:4 -n 'easy-acc-nuker'
tmux select-window -t $SESSION_NAME:nuker-task
tmux send-keys "cd $NUKER_TASK_DIR" C-m "./00_py_init.sh" C-m "source .venv/bin/activate" C-m


# go back to our main pane
tmux select-window -t $SESSION_NAME:cdk


# lets attach!
tmux attach-session -t $SESSION_NAME
